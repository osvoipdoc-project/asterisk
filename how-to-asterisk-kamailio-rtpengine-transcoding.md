# How-To: Asterisk + Kamailio + RtpEngine Transcoding #
#### Written by: Nir Simionovich, Aug 2018, Cloudonix.io

## General
### Abstract 
Transcoding is one of the main caveats of Asterisk. While significant work had been performed over the year,
no dramatic (1000%) improvements had been made. Since the early days, media transcoding was always a harsh
requirement with very little margins of operation. With the recent changes in Kamailio and RtpEngine
(mainly version 6), it is now possible to completely offload the transcoding function from Asterisk,
and let RtpEngine take care of that.

### Prerequisites 
You will need to have the following installed on your server:

  * Asterisk (I used version 15.5.0 - but you welcome to use your own)
  * Kamailio (I used 4.4.2 - it's my production version)
  * RtpEngine (I used version 6.4.1 - with the G729 codec transcoding enabled)

### Technical Data
The following had been tested inside an Amazon AWS scenario, which is meant for carrying production grade
traffic. The results incdicated very little load on the RtpEngine servers, specifically with RtpEngine 
working in Kernel mode.

## The Secret Sauce

### Your Asterisk Setup
It is imperative that your Asterisk server doesn't provide access to the codec you don't want it to 
transcode. For example, if your devices or carriers require connecting via G729, your Asterisk
server should not expose the codec at all. In my case, I've given Asterisk the most basic setup, and 
allowed it to expose only a single codec: ALAW.

#### sip.conf snippet
```
disallow=all
allow=alaw
```

Another thing you should pay attention to here is your jitterbuffer. What we've noticed is that while
in most cases a jitter buffer isn't required, if your RtpEngine is located in one VPC while your termination
is located in another, a jitter buffer is advised. We used the most basic bitter buffer setup:

```
jbenable = yes
jbforce = yes
jbmaxsize = 200
jbresyncthreshold = 1000
jbimpl = fixed
jbtargetextra = 40
```

What we've also noticed is that an "adaptive" jitter buffer can sometimes spike your machine, specifically
if your carrier is located in a jittery environment, or on the other side of the globe.

### Your RtpEngine Setup

### Your Kamailio Setup

# License

This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/legalcode or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.