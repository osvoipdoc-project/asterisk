# Informational: Asterisk Dialplan Best Practices #
#### Written by: Nir Simionovich, 2017

## General
### Abstract 
The Asterisk dialplan is the core element providing administrators and
developers the capability of developing complex voice platforms. While the usage
of the Asterisk dialplan appears to be simplistic in nature, misuse of the
dialplan can result in unmaintainable code and worse.  An insecure dialplans can
result in significant financial loss.

The purpose of this document is to provide a generalised best-practices
document, that will include various paradigms for writing and maintaining
Asterisk dialplan files at ease and securely. The two primary flavours of
dialplan are `extensions.conf` (standard dialplan) and AEL (Asterisk Extension
Language), with the former being, by far, the most common.  We will try to
provide information in both formats, to make the document as complete as
possible.

### Prerequisites 
The document assumes that you already have a working Asterisk installation and 
that you have some familiarity with the `extensions.conf` file and/or 
`extensions.ael` file formats.

### Technical Data
N/A

## RPM package prerequisites
N/A

## Standard Dialplan and AEL best practices

### Syntax is everything
Over the years, the `extensions.conf` file format had evolved. In the early 
years, the format used to look something like this:

```
exten => 1000,1,Answer
exten => 1000,2,Wait(1)
exten => 1000,3,Playback(demo-congrats)
exten => 1000,4,Hangup
```

While the above usage format is still available, it is highly discouraged. The 
above method contains *static* priority numbering, making extension of the above 
dialplan highly problematic. In addition, the usage of consecutive `exten` lines 
makes the code less readable and harder to maintain, specifically when utilizing 
multiple extensions within the same context and/or extensions file.

The following format is highly recommended:

```
exten => 1000,1,Answer
 same =>      n,Wait(1)
 same =>      n,Playback(demo-congrats)
 same =>      n,Hangup
```

The above format enables several advantages, making life for the developer much 
easier:

* The usage of the `n` (next) priority instead of a number enables insertion of new 
  lines between priorities.
* The usage of the `same` extension keyword enables the developer to create 
  extension blocks that are easily identifiable and visible

Pay careful attention to spacing, it will make your `extensions.conf` file far 
more readable.

### File Separation is Key

Modern Asterisk allows any configuration file to include other files or
directories of files.  By separating configuration files into logical groupings,
you can more easily manage them.  Moreover, separate files may have separate
permissions, allowing the system administrator to control the access to
configuration files in a layered, user-based, or role-based manner.

Some common patterns:

- One file per extension.  Since much of the standard update work on a dialplan
  involves adds, moves, and changes to discrete extensions, it can be handy to
  break out each extension to a separate file which can be safely and accurately
  manipulated without concern for interfering with other configuration data.
- Layered ingress.  Security will always be a primary concern for administrators
  of telephony systems.  Establishing a clean defense-in-depth sequence can make
  troubleshooting and gating much easier.  By layering your ingress dialplan,
  you can separate security concerns by depth and zone.

### FUNCTIONS, VARIABLES and CHANNEL VARIABLES

Asterisk functions (e.g. `TIMEOUT`, `IFTIME`, `CHANNEL`, etc.) and various other 
channel variables (`DIALSTATUS`, `DIALEDTIME`, `ANSWERTIME`, etc.) all utilize 
capital case. While there is no specific recommendation, we recommend that your 
own variables also be named in CAPITAL letters, only prefixed with a unique 2 
letter prefix.

As an example, let us examine the following dialplan sample:

```
exten => 1000,1,Answer
 same =>      n,Set(personal_timeout=30)
 same =>      n,Set(dial_timeout=120)
 same =>      n,Set(target_dial=SIP/3000)
 same =>      n,Set(TIMEOUT(absolute)=${personal_timeout})
 same =>      n,Dial(${target_dial},${dial_timeout},R)
```

There is nothing wrong with the above dialplan, it will work and it can be 
maintained at ease. However, let us examine a slightly modified version of 
the above dialplan, this time using our paradigm:

```
exten => 1000,1,Answer
 same =>      n,Set(MY_PERSONAL_TIMEOUT=30)
 same =>      n,Set(MY_DIAL_TIMEOUT=120)
 same =>      n,Set(MY_TARGET_SIP=SIP/3000)
 same =>      n,Set(TIMEOUT(absolute)=${MY_PERSONAL_TIMEOUT})
 same =>      n,Dial(${MY_TARGET_SIP},${MY_DIAL_TIMEOUT},R)
```

There is a clear and concise difference between the `absolute` parameter 
specified for the `TIMEOUT` function, while in the previous example, missing 
the `${}` combination is very easy to do and thus, misreading the dialplan. In 
addition, the usage of a prefix (`MY_`) contributes to a simpler logical reading, 
when another person reads the dialplan and immediately understands where a 
`private variable` exists.

### Stop using `Macro` - use `GoSub`
Macros were introduced to Asterisk back in version 0.9 and had remained an integral
part of the Asterisk dialplan code for many years. Macros are limited in functionality
and have severe memory limitations. The usage of Macros should be highly discouraged
and should be replaced with the `GoSub` mechanism - which is far superior and provides
a more logical development structure.

### Pay special attention when using Goto
Pay special attention when using the `Goto` dialplan application. Contrary to 
popular believe, when using `Goto` - you may change your current dialplan execution
scope. Let us examamine the following `extensions.conf` sample:

```
[context_1]
exten => 1000,1,Answer
 same =>      n(repeat),SayNumber(1000)
 same =>      n,Goto(repeat)
 
exten => 2000,1,Answer
 same =>      n(start),SayNumber(2000)
 same =>      n,Goto(1000,repeat)
 
[context_2]
exten => 1000,1,Answer
 same =>      n(repeat),SayNumber(1000)
 same =>      n,Goto(3000,start)
 
exten => 3000,1,Answer
 same =>      n(start),SayNumber(3000)
 same =>      n,Goto(context_1,2000,repeat)

```

Pay attention to the above dialplan code. Imagine that a SIP phone, which points
to the `context_2` context would now initiate a call to the extension number 1000.
The result would be that you will hear the number `1000` played back, then the 
number `3000` is played back, then the number `2000` is played back - this time 
from `context_1`. Now, the `Goto` states to go to extension `1000`, however, as
the `Goto` application change the scope of operation, we shall now go to the 
`context_1` context, although the actual activation started in `context_2`. Pay 
attention to these changes, as you jump around your dialplan code. 

Pay careful attention to this - specifically is you make usage of multiple dialplan
files, as this can get confusing very rapidly.

### Using `_` and `__` constructively
One of the lesser understood tools of the Asterisk dialplan is the usage of the 
variable "channel inheritance" operator. Asterisk includes two of these: `_`
and `__`. 

The concept of these is really simple: If you setup a new variable, prefixed with
`_`, that variable will now be available for the next forked channel. For example,
let us image the following dialplan:

```
[context_1]
exten => 1000,1,Answer
 same =>      n,Set(MY_VAR=1000)
 same =>      n,Dial(Local/3000@context_3)
 
[context_2]
exten => 2000,1,Answer
 same =>      n,Set(_MY_VAR=2000)
 same =>      n,Dial(Local/3000@context_3)

[context_3]
exten => 3000,1,Noop(**** MY_VAR is now: ${MY_VAR} ****)
 same =>      n,Hangup()
```

Should we dial `1000@context_1`, when we reach `context_3`, `MY_VAR` will have 
no idea and will print an empty string. This is caused by the fact that we forked
a call to `chan_local`, and thus, anything that was tied to the first channel is 
now gone.

Should we dial `2000@context_2`, when `context_3` is invoked, `MY_VAR` will now 
contain the number `2000`, as it was inherited from the first context.

The `_` will prmote a single fork inheritance, meaning that should you fork again,
the `MY_VAR` variable will no longer be available. 

Should you prefix `MY_VAR` with `__`, the variable will be available for all forks
of the original channel.

# License
This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/legalcode or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
