# How-To: Compile and Install Asterisk 13 with Open Source Opus Codec #
#### Written by: Nir Simionovich, 2017

## General
### Abstract 
Much had been said about installing the Asterisk Open Source PBX. This how-to document is intended to provide a means of a "best practice" guide to installing Asterisk 13, enabling pjproject and installing the Open Source Opus Code.

#### Why is Opus an important codec? 
Very few people realize it, but Opus already drives a large portion of the Internet's traffic and media services. Be it Google Hangouts or other WebRTC based services, Opus is most probably the codec being used. If you intend to use Asterisk as a base for WebRTC application, Opus is a MUST!

### Prerequisites 
Please note the following: this guide is based upon the CentOS 6.X or CentOS 7.X Linux distribution. If you are using another one, please note that package names may differ, in accordance to your flavor or Linux. 

This guide covers only a Linux based installation - you are welcome to provide a MacOS or FreeBSD guide to the project - we welcome pull requests.

### Technical Data
This document and the containing information had been tested using a [Digital Ocean](http://www.digitalocean.com) droplet.

## RPM package prerequisites
First of all, make sure to install your relevant EPEL repository. If you would like to make this a bit more automatic, use the following script to automate your process:

```bash
#!/bin/bash

# My CentOS version
MY_CENTOS_VERSION=`cat /etc/redhat-release | cut -d" " -f4 | cut -d"." -f1`
if [ $MY_CENTOS_VERSION == "(Final)" ]; then
        MY_CENTOS_VERSION=`cat /etc/redhat-release | cut -d" " -f3 | cut -d"." -f1`
fi

MY_EPEL=epel-release-$MY_CENTOS_VERSION-8.noarch.rpm
MY_EPEL_REPO=http://fedora.mirrors.romtelecom.ro/pub/epel/$MY_CENTOS_VERSION/x86_64

rpm -Uhv $MY_EPEL_REPO/$MY_EPEL
RPM_RESULT=$?
if [ $RPM_RESULT != 0]; then
  echo "Unable toe configure EPEL repository, bummer - bailing out"
  exit 99
fi

yum-config-manager --enable epel
```

For this guide to work completely, make sure the following RPM install sequence had been successfully issued on your designated server:

```sh
yum update -y
yum install -y yum-utils git bzip2 libtool pkgconfig
yum install -y gcc gcc-c++ automake autoconf uuid libuuid libuuid-devel python-devel libevent-devel make gnutls-devel kernel-devel libxml2-devel
yum install -y unbound-devel unbound-libs unbound-python
yum install -y zlib zlib-devel openssl openssl-devel ncurses ncurses-devel newt newt-devel wget
yum install -y subversion doxygen texinfo curl-devel net-snmp-devel neon-devel patch
yum install -y sqlite sqlite-devel libsrtp libsrtp-devel ngrep 
yum install -y jansson jansson-devel gsm gsm-devel speex speex-devel
yum install -y mysql mysql-server mysql-devel mysql-libs perl-DBD-MySQL fail2ban
yum install -y mysql-connector-odbc unixODBC unixODBC-devel unixODBC mysql-connector-odbc libtool-ltdl libtool-ltdl-devel
```

** Please note, the above list of YUM commands contains a list of far more RPM packages than the actual requirement. You are welcome to optimize it to your requirement. This is the set that I use on all my machines, as it enables most Asterisk requirements for most of the Asterisk Open Source projects out there in the wild.

### Step 1: Download Asterisk to /tmp
You are welcome to use the following script to make your life easier:

```bash
#!/bin/bash

MY_ASTERISK_MAJOR=13
MY_ASTERISK_MINOR=17
MY_ASTERISK_PATCHLEVEL=0
MY_ASTERISK_VERSION=$MY_ASTERISK_MAJOR.$MY_ASTERISK_MINOR.$MY_ASTERISK_PATCHLEVEL
MY_ASTERISK_DOWNLOAD_PATH=http://downloads.asterisk.org/pub/telephony/asterisk/old-releases

cd /usr/src
if [ -f /usr/src/asterisk-$MY_ASTERISK_VERSION.tar.gz ]; then
        rm -f /usr/src/asterisk-$MY_ASTERISK_VERSION.tar.gz
        rm -rf /usr/src/asterisk-$MY_ASTERISK_VERSION
fi
wget $MY_ASTERISK_DOWNLOAD_PATH/asterisk-$MY_ASTERISK_VERSION.tar.gz
tar -zxvf asterisk-$MY_ASTERISK_VERSION.tar.gz
```

The above script will download the Asterisk source code to /usr/src, unpack it and if a previous directory attempt it located in the same directory, it will clean it up. You are welcome to make this script smarter and submit a pull request.

### Step 2: Install the Opus libraries
You can either install the Opus libraries using an RPM:

```sh
yum install -y opus opus-devel
```

or, if you wish to walk on the cutting edge of Opus, you may install the libraries from source code. To do so, you are welcome to use the following script to make your life easier:

```bash
#!/bin/bash

MY_LIBOPUS_URL=http://downloads.xiph.org/releases/opus
MY_LIBOPUS_VER=1.1
MY_LIBOPUS_FILE=opus-$MY_LIBOPUS_VER
MY_LIBOPUS=$MY_LIBOPUS_URL/$MY_LIBOPUS_FILE.tar.gz

cd /usr/src
wget $MY_LIBOPUS
tar -zxvf $MY_LIBOPUS_FILE.tar.gz
cd $MY_LIBOPUS_FILE
./configure --prefix=/usr/local/opus
make && make install
touch /etc/ld.so.conf.d/opus.conf
echo "/usr/local/opus/lib" >> /etc/ld.so.conf.d/opus.conf
ldconfig

OPUS_RESULT=$?
if [ $OPUS_RESULT != 0 ]; then
    echo "An error occured compiling libopus - bailing out"
exit 99
```

** Please note, while walking on the cutting edge is encouraged for development systems, we recommend using the RPM Opus packages for production environments.

### Step 3: Patching Asterisk
Patching the Asterisk source code is a little tricky, as it may break with unwanted results. This document is based upon the Asterisk version 13.17.0 which was tested and verified. You may change the base version of Asterisk in the download script and report back errors.

To make your life easier with patching Asterisk, you are welcome to use the below script:

```bash
#!/bin/bash

MY_ASTERISK_MAJOR=13
MY_ASTERISK_MINOR=16
MY_ASTERISK_PATCHLEVEL=0
MY_ASTERISK_VERSION=$MY_ASTERISK_MAJOR.$MY_ASTERISK_MINOR.$MY_ASTERISK_PATCHLEVEL

cd /usr/src
git clone -b asterisk-13.7 https://github.com/traud/asterisk-opus.git

cd /usr/src/asterisk-$MY_ASTERISK_VERSION
cp -f /usr/src/asterisk-opus/codecs/* codecs/
cp -f /usr/src/asterisk-opus/formats/* formats/
cp -f /usr/src/asterisk-opus/include/asterisk/* include/asterisk/
cp -f /usr/src/asterisk-opus/res/* res/

patch -p1 < /usr/src/asterisk-opus/asterisk.patch
patch -p1 < /usr/src/asterisk-opus/enable_native_plc.patch

./bootstrap.sh
./configure --with-srtp --with-pjproject-bundled --without-speex
cd /usr/src/asterisk-$MY_ASTERISK_VERSION

ASTERISK_RESULT=$?
if [ $ASTERISK_RESULT != 0 ]; then
  echo "Asterisk build configuration failed, failing out - revert to manual compilation please"
  exit 99
fi
```

Make sure the above `patch` sequence executes cleanly, without any errors. If errors were detected, downgrade your Asterisk version by one minor release and try again. Optionally, if you used a source code compilation of Opus, try downgrading that one by one minor version.

### Step 4: Compiling Asterisk from source code
To compile Asterisk, once inside the Asterisk source code directory, issue the following command sequence:

```sh
make menuselect.makeopts
menuselect/menuselect --enable codec_opus_open_source menuselect.makeopts
make all && make install && make config && make samples
```

The above will enable the Opus codec for compilation and will then follow to completely compile and install the Asterisk open source PBX software.

### Step 5: Deploying init scripts 
The Asterisk deployment from source code doesn't include a straight path for deploying the Asterisk init scripts at ease. Use the following script to make your life easier in deploying the init scripts and changing Asterisk default user privilege to a user other than `root`.

```bash
#!/bin/bash

MY_ASTERISK_MAJOR=13
MY_ASTERISK_MINOR=16
MY_ASTERISK_PATCHLEVEL=0
MY_ASTERISK_VERSION=$MY_ASTERISK_MAJOR.$MY_ASTERISK_MINOR.$MY_ASTERISK_PATCHLEVEL
MY_ASTERISK_USER=apache
MY_ASTERISK_GROUP=apache


cd /usr/src/asterisk-$MY_ASTERISK_VERSION

# Deploying init script
cp contrib/init.d/rc.redhat.asterisk /tmp/asterisk.init.v1
sed 's/__ASTERISK_SBIN_DIR__/\/usr\/sbin/' /tmp/asterisk.init.v1 > /tmp/asterisk.init.v2
sed 's/#AST_CONFIG/AST_CONFIG/' /tmp/asterisk.init.v2 > /tmp/asterisk.init.v1
sed 's/__ASTERISK_ETC_DIR__/\/etc\/asterisk/' /tmp/asterisk.init.v1 > /tmp/asterisk.init.v2
cp /tmp/asterisk.init.v2 /etc/init.d/asterisk
chmod +x /etc/init.d/asterisk
chkconfig --add asterisk
chkconfig --level 35 asterisk on

# Fixing permissions
sed "s/;runuser = asterisk/runuser = $MY_ASTERISK_USER/" /etc/asterisk/asterisk.conf > /tmp/asterisk.conf.v1
sed "s/;rungroup = asterisk/rungroup = $MY_ASTERISK_GROUP/" /tmp/asterisk.conf.v1 > /tmp/asterisk.conf.v2
cp -f /tmp/asterisk.conf.v2 /etc/asterisk/asterisk.conf

```

### Step 6: Testing and verification
To test your installation, issue the following command to start your Asterisk in the foreground:

```sh
asterisk -vvvcgp
```

Once inside the Asterisk console, issue the following command:

```
core show translation
```

The screen should emit the list of available codecs inside Asterisk, which should resemble the following:

```
         Translation times between formats (in microseconds) for one second of data
          Source Format (Rows) Destination Format (Columns)

           g723  ulaw  alaw   gsm  g726 g726aal2 adpcm  slin  slin  slin  slin  slin  slin  slin  slin  slin lpc10  g729  ilbc  g722 testlaw  opus
     g723     - 15000 15000 15000 15000    15000 15000  9000 17000 17000 17000 17000 17000 17000 17000 17000 15000 15000 15000 17250   15000 15000
     ulaw 15000     -  9150 15000 15000    15000 15000  9000 17000 17000 17000 17000 17000 17000 17000 17000 15000 15000 15000 17250   15000 15000
     alaw 15000  9150     - 15000 15000    15000 15000  9000 17000 17000 17000 17000 17000 17000 17000 17000 15000 15000 15000 17250   15000 15000
      gsm 15000 15000 15000     - 15000    15000 15000  9000 17000 17000 17000 17000 17000 17000 17000 17000 15000 15000 15000 17250   15000 15000
     g726 15000 15000 15000 15000     -    15000 15000  9000 17000 17000 17000 17000 17000 17000 17000 17000 15000 15000 15000 17250   15000 15000
 g726aal2 15000 15000 15000 15000 15000        - 15000  9000 17000 17000 17000 17000 17000 17000 17000 17000 15000 15000 15000 17250   15000 15000
    adpcm 15000 15000 15000 15000 15000    15000     -  9000 17000 17000 17000 17000 17000 17000 17000 17000 15000 15000 15000 17250   15000 15000
     slin  6000  6000  6000  6000  6000     6000  6000     -  8000  8000  8000  8000  8000  8000  8000  8000  6000  6000  6000  8250    6000  6000
     slin 14500 14500 14500 14500 14500    14500 14500  8500     -  8000  8000  8000  8000  8000  8000  8000 14500 14500 14500 14000   14500  5999
     slin 14500 14500 14500 14500 14500    14500 14500  8500  8500     -  8000  8000  8000  8000  8000  8000 14500 14500 14500  6000   14500  5999
     slin 14500 14500 14500 14500 14500    14500 14500  8500  8500  8500     -  8000  8000  8000  8000  8000 14500 14500 14500 14500   14500  5999
     slin 14500 14500 14500 14500 14500    14500 14500  8500  8500  8500  8500     -  8000  8000  8000  8000 14500 14500 14500 14500   14500 13999
     slin 14500 14500 14500 14500 14500    14500 14500  8500  8500  8500  8500  8500     -  8000  8000  8000 14500 14500 14500 14500   14500 13999
     slin 14500 14500 14500 14500 14500    14500 14500  8500  8500  8500  8500  8500  8500     -  8000  8000 14500 14500 14500 14500   14500  5999
     slin 14500 14500 14500 14500 14500    14500 14500  8500  8500  8500  8500  8500  8500  8500     -  8000 14500 14500 14500 14500   14500 14499
     slin 14500 14500 14500 14500 14500    14500 14500  8500  8500  8500  8500  8500  8500  8500  8500     - 14500 14500 14500 14500   14500 14499
    lpc10 15000 15000 15000 15000 15000    15000 15000  9000 17000 17000 17000 17000 17000 17000 17000 17000     - 15000 15000 17250   15000 15000
     g729 15000 15000 15000 15000 15000    15000 15000  9000 17000 17000 17000 17000 17000 17000 17000 17000 15000     - 15000 17250   15000 15000
     ilbc 15000 15000 15000 15000 15000    15000 15000  9000 17000 17000 17000 17000 17000 17000 17000 17000 15000 15000     - 17250   15000 15000
     g722 15600 15600 15600 15600 15600    15600 15600  9600 17500  9000 17000 17000 17000 17000 17000 17000 15600 15600 15600     -   15600 14999
  testlaw 15000 15000 15000 15000 15000    15000 15000  9000 17000 17000 17000 17000 17000 17000 17000 17000 15000 15000 15000 17250       - 15000
     opus 15000 15000 15000 15000 15000    15000 15000  9000  8999  8999  8999 16999 16999  8999 16999 16999 15000 15000 15000 14999   15000     -
```

Pay specific attention to the columns and rows names `opus`, which indicate that Opus is now available on your Asterisk server.

# License

This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/legalcode or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.