# How-To: Asterisk CDR/CEL and beanstalkd #
#### Written by: Nir Simionovich, 2017

## General
### Abstract 

### Prerequisites 

### Technical Data

## RPM package prerequisites

## The theory of using `beanstalkd`

## `cdr_beanstalkd.conf`

## `cel_beanstalkd.conf`

# License


This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/legalcode or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.