# Asterisk

[Asterisk](http://asterisk.org) is probably the best known open-source PBX-style
phone systems.  It can be deployed to function as an office telephone system as
well as a voice application and (telephony) media server.

The configuration of Asterisk is done through a set of configuration files which
are roughly module-oriented.  The average use does not need to touch most of
them, but can usually safely start with the [basic-pbx](https://github.com/asterisk/asterisk/tree/master/configs/basic-pbx) configuration set.

The configuration files can be roughly grouped:

  - `core` configurations, such as `asterisk.conf` and `modules.conf`, which
    configure the core of Asterisk, how it runs, and how it loads and uses other
    modules.
  - `channel` configurations, such as `pjsip.conf` and `chan_dahdi.conf`, which
    control how external telephony resources communicate with Asterisk.
  - `resource` configurations, such as `res_odbc.conf` and `res_curl.conf`,
    which configure how Asterisk utilizes various internal and external resource
    adapters.
  - There are many other miscellaneous configuration files, as well, such as the
    CDR and CEL configurations (each one represents a different type of call logging
    backend).

The flow of a phone call through the system is determined by the dialplan.
While Asterisk provides several methods by which the dialplan may be defined, by
far the most common (and generally considered the simplest) is the
`extensions.conf` dialplan.  See the Dial Plan item in the [Best Practices](#best-practices)
section below for more information.

In this repository, we combine various best-practice documents and high-level
descriptions of features and components in order to supplement the existing
reference materials available on the official [Asterisk
Wiki](https://wiki.asterisk.org/).

## Features

...

## Useful Scripts

### RPM SPEC Files

 - [Asterisk 14.7.2 with Open Source Opus](./useful-scripts/RPM_spec_files/asterisk-open-source-opus.spec)

## Best Practices

 - [Dial Plan](./asterisk-dialplan-best-practices.md)

## How-To's

 - [Adding an External Library](./how-to-add-a-new-external-library-to-asterisk-build.md)
 - [Opus Codec](./how-to-asterisk-13-and-open-source-opus.md) (Asterisk 13)

