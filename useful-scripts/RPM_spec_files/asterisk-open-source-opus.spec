###################################################################
#
#  asterisk.spec - used to generate asterisk rpm
#  For more info: http://www.rpm.org/max-rpm/ch-rpm-basics.html
#
#  This spec file uses default directories:
#  /usr/src/redhat/SOURCES - orig source, patches, icons, etc.
#  /usr/src/redhat/SPECS - spec files
#  /usr/src/redhat/BUILD - sources unpacked & built here
#  /usr/src/redhat/RPMS - binary package files
#  /usr/src/redhat/SRPMS - source package files
#
###################################################################
#
#  Global Definitions
#
###################################################################
%define aversion 14.7.2
%define arelease 2%{?dist}
%define actversion %(echo %{aversion}|sed -e "s/-.*$//g")
%define subvers %(echo %{aversion}|awk "/-/"|sed -e "s/^.*-//"|awk '{print "0." $1 "."}')
%define actrelease %(echo %{subvers}%{arelease}|sed -e "s/-/_/g")

%define astapi 14

%define ast_opus 13.7

#%define docdir /usr/share/doc/asterisk
%define logdir /var/log

%define _without_dahdi 1
%define _without_misdn 1
%define _without_resample 1
%bcond_without opus

%{?el5:%define _lib lib}

###################################################################
#
#  The Preamble
#  information that is displayed when users request info
#
###################################################################
Summary: Asterisk, The Open Source PBX
Name: asterisk
Version: %{actversion}
Release: %{actrelease}%{?_without_optimizations:_debug}
License: GPL
Group: Utilities/System
Source: https://downloads.asterisk.org/pub/telephony/asterisk/releases/%{name}-%{aversion}.tar.gz
Source2: asterisk.logrotate
Source3: https://github.com/traud/asterisk-opus/archive/asterisk-%{ast_opus}/asterisk-opus-%{ast_opus}.tar.gz
Patch2: voicemail-splitopts.patch
Patch3: voicemail-splitopts-odbcstorage.patch
Patch4: voicemail-splitopts-imapstorage.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-root
URL: http://www.asterisk.org
Vendor: Digium, Inc.
Packager: Asterisk Development Team <asteriskteam@digium.com>
# Uncomment when we're ready to be the default.
#Provides: asterisk
#Obsoletes: asterisk
Conflicts: asterisk15
Conflicts: asterisk13
Conflicts: asterisk16
Conflicts: asterisk18
Conflicts: asterisk10
Conflicts: asterisk11
Provides: asterisk%{astapi}
Obsoletes: asterisk%{astapi}

#BuildRequires: asterisk-osp-devel

%{?_without_optimizations:Requires: %{name}-debuginfo = %{actversion}-%{release}}
Requires: %{name}-core = %{actversion}-%{release}
%{!?_without_dahdi:Requires: %{name}-dahdi = %{actversion}-%{release}}
Requires: %{name}-doc = %{actversion}
Requires: %{name}-voicemail = %{actversion}-%{release}
Requires: asterisk-sounds-core-en-gsm
BuildRequires: sqlite-devel
BuildRequires: ncurses-devel
BuildRequires: libxml2-devel
BuildRequires: iksemel-devel
BuildRequires: jansson-devel
BuildRequires: libuuid-devel
BuildRequires: libxslt-devel
#BuildRequires: pjproject-devel
BuildRequires: libsrtp-devel
BuildRequires: spandsp-devel
BuildRequires: openldap-devel
BuildRequires: gmime-devel
BuildRequires: lua-devel
BuildRequires: portaudio-devel
BuildRequires: libical-devel
BuildRequires: neon-devel
BuildRequires: radiusclient-ng-devel
%{!?_without_newt:BuildRequires: newt-devel}
%{?_with_opus:BuildRequires: opus-devel}
BuildConflicts: rh-postgresql-devel

# svn is needed for get_mp3_source.sh
BuildRequires: subversion

#
# Other tags not used
#
#Distribution:
#Icon:
#Conflicts:
#Serial:
#Provides:
#AutoReqProv:
#ExcludeArch:

%description
Asterisk is an open source PBX and telephony development platform.  Asterisk
can both replace a conventional PBX and act as a platform for the
development of custom telephony applications for the delivery of dynamic
content over a telephone; similar to how one can deliver dynamic content
through a web browser using CGI and a web server.

Asterisk supports a variety of telephony hardware including BRI, PRI, POTS,
and IP telephony clients using the Inter-Asterisk eXchange (IAX) protocol (e.g.
gnophone or miniphone).  For more information and a current list of supported
hardware, see http://www.asterisk.org

#
#  core subpackage
#
%package core
Summary: Asterisk core package without any "extras".
Group: Utilities/System
Provides: asterisk%{astapi}-core
Obsoletes: asterisk%{astapi}-core
#Requires: asterisknow-version
Requires: openssl
Requires: libxml2
Requires: sqlite
Requires: iksemel
Requires: jansson
Requires: libuuid
Requires: libxslt
#Requires: pjproject

%description core
This package contains a base install of Asterisk without any "extras".

#
#  Alsa subpackage
#
%{?_without_alsa:%if 0}
%{!?_without_alsa:%if 1}
%package alsa
Summary: Alsa channel driver for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-alsa
Obsoletes: asterisk%{astapi}-alsa
%if 0%{?suse_version}
BuildRequires: alsa-devel
Requires: alsa
%else
BuildRequires: alsa-lib-devel
Requires: alsa-lib
%endif
Requires: %{name}-core = %{actversion}-%{release}

%description alsa
Alsa channel driver for Asterisk
%endif

#
#  snmp subpackage
#
%{?_without_snmp:%if 0}
%{!?_without_snmp:%if 1}
%package snmp
Summary: snmp resource module for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-snmp
Obsoletes: asterisk%{astapi}-snmp
%if 0%{?suse_version}
BuildRequires: sensors
BuildRequires: tcpd-devel
%else
BuildRequires: lm_sensors-devel
%endif
BuildRequires: net-snmp-devel
Requires: net-snmp
Requires: %{name}-core = %{actversion}-%{release}

%description snmp
snmp resource module for Asterisk
%endif

#
#  pgsql subpackage
#
%{?_without_pgsql:%if 0}
%{!?_without_pgsql:%if 1}
%package pgsql
Summary: Postgresql modules for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-pgsql
Obsoletes: asterisk%{astapi}-pgsql
BuildRequires: postgresql-devel
Requires: postgresql
Requires: %{name}-core = %{actversion}-%{release}

%description pgsql
Postgresql modules for Asterisk
%endif

#
#  tds subpackage
#
%{?_without_tds:%if 0}
%{!?_without_tds:%if 1}
%package tds
Summary: tds modules for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-tds
Obsoletes: asterisk%{astapi}-tds
BuildRequires: freetds-devel
Requires: freetds
Requires: %{name}-core = %{actversion}-%{release}

%description tds
tds modules for Asterisk
%endif

#
#  dahdi subpackage
#
%{?_without_dahdi:%if 0}
%{!?_without_dahdi:%if 1}
%package dahdi
Summary: DAHDI channel driver for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-dahdi
Obsoletes: asterisk%{astapi}-dahdi
Requires: %{name}-core = %{actversion}-%{release}
BuildRequires: libopenr2-devel
BuildRequires: libpri-devel
BuildRequires: libss7-devel
BuildRequires: libtonezone-devel
BuildRequires: dahdi-linux-devel
Requires: libopenr2
Requires: libpri
Requires: libss7
Requires: libtonezone
Requires: dahdi-linux
Requires: dahdi-linux-kmod
AutoReqProv: no

%description dahdi
DAHDI channel driver for Asterisk
%endif

#
#  mISDN subpackage
#
%{?_without_misdn:%if 0}
%{!?_without_misdn:%if 1}
%package misdn
Summary: mISDN channel driver for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-misdn
Obsoletes: asterisk%{astapi}-misdn
Requires: %{name}-core = %{actversion}-%{release}
BuildRequires: mISDNuser-devel
BuildRequires: mISDN-devel
Requires: mISDNuser
Requires: mISDN
Requires: mISDN-kmod
AutoReqProv: no

%description misdn
mISDN channel driver for Asterisk
%endif

#
#  Configs subpackage
#
%package configs
Summary: Basic configuration files for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-configs
Obsoletes: asterisk%{astapi}-configs
Requires: %{name}-core = %{actversion}-%{release}

%description configs
The sample configuration files for Asterisk

#
#  cURL subpackage
#
%{?_without_curl:%if 0}
%{!?_without_curl:%if 1}
%package curl
Summary: cURL application module for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-curl
Obsoletes: asterisk%{astapi}-curl
BuildRequires: curl-devel
Requires: curl
Requires: %{name}-core = %{actversion}-%{release}

%description curl
cURL application module for Asterisk
%endif

#
#  Development subpackage
#
%package devel
Summary: Static libraries and header files for Asterisk development
Group: Development/Libraries
Provides: asterisk%{astapi}-devel
Obsoletes: asterisk%{astapi}-devel
Requires: %{name}-core = %{actversion}-%{release}

%description devel
The static libraries and header files needed for building additional Asterisk
plugins/modules

#
#  Documentation subpackage
#
%package doc
Summary: Documentation files for Asterisk
Group: Development/Libraries
Provides: asterisk%{astapi}-doc
Obsoletes: asterisk%{astapi}-doc
Requires: %{name}-core = %{actversion}-%{release}

%description doc
The Documentation files for Asterisk

#
#  Ogg-Vorbis subpackage
#
%{?_without_ogg:%if 0}
%{!?_without_ogg:%if 1}
%package ogg
Summary: Ogg-Vorbis codec module for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-ogg
Obsoletes: asterisk%{astapi}-ogg
BuildRequires: libvorbis-devel libogg-devel
Requires: libvorbis libogg
Requires: %{name}-core = %{actversion}-%{release}

%description ogg
Asterisk format plugin for the Ogg-Vorbis codec
%endif

#
#  Speex subpackage
#
%{?_without_speex:%if 0}
%{!?_without_speex:%if 1}
%package speex
Summary: Speex codec module for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-speex
Obsoletes: asterisk%{astapi}-speex
BuildRequires: speex-devel
Requires: speex
Requires: %{name}-core = %{actversion}-%{release}

%description speex
Asterisk format plugin for the Speex codec
%endif

#
#  Opus subpackage
#
%if %{with opus}
%package opus
Summary: Opus codec module for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-opus
Obsoletes: asterisk%{astapi}-opus
BuildRequires: opus-devel
Requires: opus
Requires: %{name}-core = %{actversion}-%{release}

%description opus
Asterisk plugin for the Opus codec
%endif

#
#  resample subpackage
#
%{?_without_resample:%if 0}
%{!?_without_resample:%if 1}
%package resample
Summary: resampling codec module for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-resample
Obsoletes: asterisk%{astapi}-resample
BuildRequires: libresample-devel
Requires: libresample
Requires: %{name}-core = %{actversion}-%{release}

%description resample
Asterisk plugin for the resample codec
%endif

#
#  unixODBC subpackage
#
%{?_without_odbc:%if 0}
%{!?_without_odbc:%if 1}
%package odbc
Summary: Open Database Connectivity (ODBC) drivers for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-odbc
Obsoletes: asterisk%{astapi}-odbc
BuildRequires: unixODBC-devel
BuildRequires: libtool-ltdl-devel
Requires: unixODBC
Requires: %{name}-core = %{actversion}-%{release}

%description odbc
ODBC drivers for Asterisk
%endif

#
#  sqlite3 subpackage
#
%{?_without_sqlite3:%if 0}
%{!?_without_sqlite3:%if 1}
%package sqlite3
Summary: sqlite3 drivers for Asterisk
Group: Utilities/System
Provides: asterisk%{astapi}-sqlite3
Obsoletes: asterisk%{astapi}-sqlite3
BuildRequires: sqlite-devel
Requires: sqlite
Requires: %{name}-core = %{actversion}-%{release}

%description sqlite3
sqlite3 drivers for Asterisk
%endif

#
#  voicemail file storage subpackage
#
%package voicemail
Summary: Voicemail with file storage module for Asterisk
Group: Utilities/System
Provides: %{name}-voicemail-filestorage = %{actversion}-%{release}
Provides: asterisk%{astapi}-voicemail
Obsoletes: asterisk%{astapi}-voicemail
Provides: asterisk%{astapi}-voicemail-filestorage
Requires: %{name}-core = %{actversion}-%{release}

Conflicts: %{name}-voicemail-odbcstorage
Conflicts: %{name}-voicemail-imapstorage

%description voicemail
Voicemail with file storage module for Asterisk

#
#  voicemail ODBC storage subpackage
#
%{?_without_voicemail_odbcstorage:%if 0}
%{!?_without_voicemail_odbcstorage:%if 1}
%package voicemail-odbcstorage
Summary: Voicemail with ODBC storage module for Asterisk
Group: Utilities/System
BuildRequires: unixODBC-devel
Requires: unixODBC
Requires: %{name}-core = %{actversion}-%{release}
Provides: %{name}-voicemail = %{actversion}-%{release}
Provides: asterisk%{astapi}-voicemail
Provides: asterisk%{astapi}-voicemail-odbcstorage
Obsoletes: asterisk%{astapi}-voicemail-odbcstorage

Conflicts: %{name}-voicemail-filestorage
Conflicts: %{name}-voicemail-imapstorage

%description voicemail-odbcstorage
Voicemail with ODBC storage module for Asterisk
%endif

#
#  voicemail IMAP storage subpackage
#
%{?_without_voicemail_imapstorage:%if 0}
%{!?_without_voicemail_imapstorage:%if 1}
%package voicemail-imapstorage
Summary: Voicemail with IMAP storage module for Asterisk
Group: Utilities/System
%if 0%{?suse_version}
BuildRequires: imap-devel
Requires: imap-lib
%else
BuildRequires: libc-client-devel
Requires: libc-client
%endif
Requires: %{name}-core = %{actversion}-%{release}
Provides: %{name}-voicemail = %{actversion}-%{release}
Provides: asterisk%{astapi}-voicemail
Provides: asterisk%{astapi}-voicemail-imapstorage
Obsoletes: asterisk%{astapi}-voicemail-imapstorage

Conflicts: %{name}-voicemail-filestorage
Conflicts: %{name}-voicemail-odbcstorage

%description voicemail-imapstorage
Voicemail with IMAP storage module for Asterisk
%endif

#
#  addons subpackage
#
%package addons
Summary: Asterisk-addons package.
Group: Utilities/System
Requires: asterisk%{astapi}-addons-core = %{actversion}-%{release}
Provides: asterisk-addons
Provides: asterisk%{astapi}-addons

Requires: %{name}-addons-core = %{actversion}-%{release}

%{?_without_mysql:%if 0}
%{!?_without_mysql:%if 1}
Requires: %{name}-addons-mysql = %{actversion}-%{release}
Requires: mysql
%endif

%{?_without_bluetooth:%if 0}
%{!?_without_bluetooth:%if 1}
Requires: %{name}-addons-bluetooth = %{actversion}-%{release}
Requires: bluez-libs
%endif

%{?_without_ooh323:%if 0}
%{!?_without_ooh323:%if 1}
Requires: %{name}-addons-ooh323 = %{actversion}-%{release}
%endif

%description addons
This package contains a base install of Asterisk-addons without any "extras".

#
#  addons-core subpackage
#
%package addons-core
Summary: Asterisk-addons core package.
Group: Utilities/System
Provides: asterisk-gplonly
Requires: asterisk%{astapi}-core = %{actversion}-%{release}
Provides: asterisk-addons-core
Provides: asterisk%{astapi}-addons-core

%description addons-core
This package contains a base install of Asterisk-addons without any "extras".

#
#  addons-mysql subpackage
#
%{?_without_mysql:%if 0}
%{!?_without_mysql:%if 1}
%package addons-mysql
Summary: mysql modules for Asterisk
Group: Utilities/System
BuildRequires: mysql-devel
Requires: mysql
Requires: %{name}-addons-core = %{actversion}-%{release}
Provides: asterisk-addons-mysql
Provides: asterisk%{astapi}-addons-mysql

%description addons-mysql
mysql modules for Asterisk
%endif

#
#  bluetooth subpackage
#
%{?_without_bluetooth:%if 0}
%{!?_without_bluetooth:%if 1}
%package addons-bluetooth
Summary: bluetooth modules for Asterisk
Group: Utilities/System
BuildRequires: bluez-libs-devel
Requires: bluez-libs
Requires: %{name}-core = %{actversion}-%{release}
Provides: asterisk-addons-bluetooth
Provides: asterisk%{astapi}-addons-bluetooth

%description addons-bluetooth
bluetooth modules for Asterisk
%endif

#
#  ooh323 subpackage
#
%{?_without_ooh323:%if 0}
%{!?_without_ooh323:%if 1}
%package addons-ooh323
Summary: chan_ooh323 module for Asterisk
Group: Utilities/System
Requires: %{name}-core = %{actversion}-%{release}
Provides: asterisk-addons-ooh323
Provides: asterisk%{astapi}-addons-ooh323

%description addons-ooh323
chan_ooh323 module for Asterisk
%endif


###################################################################
#
#  The Prep Section
#  If stuff needs to be done before building, this is the section
#  Use shell scripts to do stuff like uncompress and cd into source dir
#  %setup macro - cleans old build trees, uncompress and extracts original source
#
###################################################################
%prep
%setup -n %{name}-%{aversion}
%setup -T -D -a 3

# Create copies for odbcstorage and imapstorage voicemail modules.
%{?_without_voicemail_odbcstorage:%if 0}
%{!?_without_voicemail_odbcstorage:%if 1}
cp apps/app_voicemail.c apps/app_voicemail_odbcstorage.c
cp apps/app_voicemail.exports.in apps/app_voicemail_odbcstorage.exports.in
%patch3 -p0
%endif

%{?_without_voicemail_imapstorage:%if 0}
%{!?_without_voicemail_imapstorage:%if 1}
cp apps/app_voicemail.c apps/app_voicemail_imapstorage.c
cp apps/app_voicemail.exports.in apps/app_voicemail_imapstorage.exports.in
%patch4 -p0
%endif

%if %{with opus}
pwd
cp -R asterisk-opus-asterisk-%{ast_opus}/codecs/* codecs/
cp -R asterisk-opus-asterisk-%{ast_opus}/formats/* formats/
cp -R asterisk-opus-asterisk-%{ast_opus}/include/* include/
cp -R asterisk-opus-asterisk-%{ast_opus}/res/* res/
patch -p1 < asterisk-opus-asterisk-%{ast_opus}/asterisk.patch
patch -p1 < asterisk-opus-asterisk-%{ast_opus}/enable_native_plc.patch
%endif

%patch2 -p0

./bootstrap.sh

###################################################################
#
#  The Build Section
#  Use shell scripts and do stuff that makes the build happen, i.e. make
#
###################################################################
%build
%ifarch x86_64
%define makeflags OPT=-fPIC
%else
%define makeflags OPT=
%endif
echo %{aversion}%{?_without_optimizations:-debug} > .version

%{configure} --with-pjproject-bundled --with-srtp
make menuselect.makeopts

%if %{with opus}
menuselect/menuselect --disable-category MENUSELECT_CORE_SOUNDS --disable-category MENUSELECT_EXTRA_SOUNDS --disable-category MENUSELECT_MOH --enable-category MENUSELECT_ADDONS --enable app_meetme --enable app_page --disable chan_mgcp --enable codec_opus_open_source menuselect.makeopts
%else
menuselect/menuselect --disable-category MENUSELECT_CORE_SOUNDS --disable-category MENUSELECT_EXTRA_SOUNDS --disable-category MENUSELECT_MOH --enable-category MENUSELECT_ADDONS --enable app_meetme --enable app_page --disable chan_mgcp menuselect.makeopts
%endif

contrib/scripts/get_mp3_source.sh

make %{?_smp_mflags} %{makeflags}

###################################################################
#
#  The Install Section
#  Use shell scripts and perform the install, like 'make install',
#  but can also be shell commands, i.e. cp, mv, install, etc..
#
###################################################################
%install
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/rc.d/init.d/
mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/sysconfig/
echo "AST_USER=asterisk" > $RPM_BUILD_ROOT/%{_sysconfdir}/sysconfig/asterisk
echo "AST_GROUP=asterisk" >> $RPM_BUILD_ROOT/%{_sysconfdir}/sysconfig/asterisk

make DESTDIR=$RPM_BUILD_ROOT install
make DESTDIR=$RPM_BUILD_ROOT samples
make DESTDIR=$RPM_BUILD_ROOT config

mkdir -p $RPM_BUILD_ROOT/%{_sysconfdir}/logrotate.d/
cp %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/logrotate.d/asterisk

mkdir -p $RPM_BUILD_ROOT/var/lib/asterisk/licenses/
mkdir -p $RPM_BUILD_ROOT/var/lib/digium/licenses/

###################################################################
#
#  Install and Uninstall
#  This section can have scripts that are run either before/after
#  an install process, or before/after an uninstall process
#  %pre - executes prior to the installation of a package
#  %post - executes after the package is installed
#  %preun - executes prior to the uninstallation of a package
#  %postun - executes after the uninstallation of a package
#
###################################################################
%pre core
useradd -r -M -s /sbin/nologin -d /var/lib/asterisk asterisk 2> /dev/null || :

%post core
chkconfig --add asterisk

###################################################################
#
#  Verify
#
###################################################################
%verifyscript

###################################################################
#
#  Clean
#
###################################################################
%clean
%{__rm} -rf $RPM_BUILD_ROOT

###################################################################
#
#  File List
#
###################################################################
%files
%defattr(-, root, root)
%exclude /var/lib/asterisk/scripts/

%files core
%defattr(-, root, root)
%config %{_sysconfdir}/rc.d/init.d/asterisk
%config %{_sysconfdir}/sysconfig/asterisk
%attr(0775,asterisk,asterisk) %dir %{_sysconfdir}/asterisk
#
#  Module List
#
%{_libdir}/asterisk/modules/app_adsiprog.so
%{_libdir}/asterisk/modules/app_agent_pool.so
%{_libdir}/asterisk/modules/app_alarmreceiver.so
%{_libdir}/asterisk/modules/app_amd.so
%{_libdir}/asterisk/modules/app_authenticate.so
%{_libdir}/asterisk/modules/app_bridgewait.so
%{_libdir}/asterisk/modules/app_cdr.so
%{_libdir}/asterisk/modules/app_celgenuserevent.so
%{_libdir}/asterisk/modules/app_chanisavail.so
%{_libdir}/asterisk/modules/app_channelredirect.so
%{_libdir}/asterisk/modules/app_chanspy.so
%{_libdir}/asterisk/modules/app_confbridge.so
%{_libdir}/asterisk/modules/app_controlplayback.so
%{_libdir}/asterisk/modules/app_db.so
%{_libdir}/asterisk/modules/app_dial.so
%{_libdir}/asterisk/modules/app_dictate.so
%{_libdir}/asterisk/modules/app_directed_pickup.so
%{_libdir}/asterisk/modules/app_directory.so
%{_libdir}/asterisk/modules/app_disa.so
%{_libdir}/asterisk/modules/app_dumpchan.so
%{_libdir}/asterisk/modules/app_echo.so
%{_libdir}/asterisk/modules/app_exec.so
%{_libdir}/asterisk/modules/app_externalivr.so
%{_libdir}/asterisk/modules/app_festival.so
%{_libdir}/asterisk/modules/app_forkcdr.so
%{_libdir}/asterisk/modules/app_followme.so
%{_libdir}/asterisk/modules/app_getcpeid.so
%{_libdir}/asterisk/modules/app_ices.so
%{_libdir}/asterisk/modules/app_image.so
%{_libdir}/asterisk/modules/app_macro.so
%{_libdir}/asterisk/modules/app_milliwatt.so
%{_libdir}/asterisk/modules/app_minivm.so
%{_libdir}/asterisk/modules/app_mixmonitor.so
%{_libdir}/asterisk/modules/app_morsecode.so
%{_libdir}/asterisk/modules/app_mp3.so
%{_libdir}/asterisk/modules/app_nbscat.so
%{_libdir}/asterisk/modules/app_originate.so
%{_libdir}/asterisk/modules/app_page.so
%{_libdir}/asterisk/modules/app_playback.so
%{_libdir}/asterisk/modules/app_playtones.so
%{_libdir}/asterisk/modules/app_privacy.so
%{_libdir}/asterisk/modules/app_queue.so
%{_libdir}/asterisk/modules/app_readexten.so
%{_libdir}/asterisk/modules/app_read.so
%{_libdir}/asterisk/modules/app_record.so
%{_libdir}/asterisk/modules/app_sayunixtime.so
%{_libdir}/asterisk/modules/app_senddtmf.so
%{_libdir}/asterisk/modules/app_sendtext.so
%{_libdir}/asterisk/modules/app_sms.so
%{_libdir}/asterisk/modules/app_softhangup.so
%{_libdir}/asterisk/modules/app_speech_utils.so
%{_libdir}/asterisk/modules/app_stack.so
%{_libdir}/asterisk/modules/app_stasis.so
%{_libdir}/asterisk/modules/app_system.so
%{_libdir}/asterisk/modules/app_talkdetect.so
%{_libdir}/asterisk/modules/app_test.so
%{_libdir}/asterisk/modules/app_transfer.so
%{_libdir}/asterisk/modules/app_url.so
%{_libdir}/asterisk/modules/app_userevent.so
%{_libdir}/asterisk/modules/app_verbose.so
%{_libdir}/asterisk/modules/app_waitforring.so
%{_libdir}/asterisk/modules/app_waitforsilence.so
%{_libdir}/asterisk/modules/app_waituntil.so
%{_libdir}/asterisk/modules/app_while.so
%{_libdir}/asterisk/modules/app_zapateller.so
%{_libdir}/asterisk/modules/bridge_builtin_features.so
%{_libdir}/asterisk/modules/bridge_builtin_interval_features.so
%{_libdir}/asterisk/modules/bridge_holding.so
%{_libdir}/asterisk/modules/bridge_native_rtp.so
%{_libdir}/asterisk/modules/bridge_simple.so
%{_libdir}/asterisk/modules/bridge_softmix.so
%{_libdir}/asterisk/modules/cdr_csv.so
%{_libdir}/asterisk/modules/cdr_custom.so
%{_libdir}/asterisk/modules/cdr_manager.so
%{_libdir}/asterisk/modules/cdr_syslog.so
%{_libdir}/asterisk/modules/cel_custom.so
%{_libdir}/asterisk/modules/cel_manager.so
%{_libdir}/asterisk/modules/chan_bridge_media.so
%{_libdir}/asterisk/modules/chan_iax2.so
%{_libdir}/asterisk/modules/chan_motif.so
%{_libdir}/asterisk/modules/chan_oss.so
%{_libdir}/asterisk/modules/chan_phone.so
%{_libdir}/asterisk/modules/chan_pjsip.so
%{_libdir}/asterisk/modules/chan_skinny.so
%{_libdir}/asterisk/modules/chan_sip.so
%{_libdir}/asterisk/modules/chan_unistim.so
%{_libdir}/asterisk/modules/codec_adpcm.so
%{_libdir}/asterisk/modules/codec_alaw.so
%{_libdir}/asterisk/modules/codec_a_mu.so
%{_libdir}/asterisk/modules/codec_g722.so
%{_libdir}/asterisk/modules/codec_g726.so
%{_libdir}/asterisk/modules/codec_gsm.so
%{_libdir}/asterisk/modules/codec_ilbc.so
%{_libdir}/asterisk/modules/codec_lpc10.so
%{_libdir}/asterisk/modules/codec_speex.so
%{_libdir}/asterisk/modules/codec_ulaw.so
%{_libdir}/asterisk/modules/format_g719.so
%{_libdir}/asterisk/modules/format_g723.so
%{_libdir}/asterisk/modules/format_g726.so
%{_libdir}/asterisk/modules/format_g729.so
%{_libdir}/asterisk/modules/format_gsm.so
%{_libdir}/asterisk/modules/format_h263.so
%{_libdir}/asterisk/modules/format_h264.so
%{_libdir}/asterisk/modules/format_ilbc.so
%{_libdir}/asterisk/modules/format_jpeg.so
%{_libdir}/asterisk/modules/format_pcm.so
%{_libdir}/asterisk/modules/format_siren7.so
%{_libdir}/asterisk/modules/format_siren14.so
%{_libdir}/asterisk/modules/format_sln.so
%{_libdir}/asterisk/modules/format_vox.so
%{_libdir}/asterisk/modules/format_wav_gsm.so
%{_libdir}/asterisk/modules/format_wav.so
%{_libdir}/asterisk/modules/func_aes.so
%{_libdir}/asterisk/modules/func_audiohookinherit.so
%{_libdir}/asterisk/modules/func_base64.so
%{_libdir}/asterisk/modules/func_blacklist.so
%{_libdir}/asterisk/modules/func_callcompletion.so
%{_libdir}/asterisk/modules/func_callerid.so
%{_libdir}/asterisk/modules/func_cdr.so
%{_libdir}/asterisk/modules/func_channel.so
%{_libdir}/asterisk/modules/func_config.so
%{_libdir}/asterisk/modules/func_cut.so
%{_libdir}/asterisk/modules/func_db.so
%{_libdir}/asterisk/modules/func_devstate.so
%{_libdir}/asterisk/modules/func_dialgroup.so
%{_libdir}/asterisk/modules/func_dialplan.so
%{_libdir}/asterisk/modules/func_enum.so
%{_libdir}/asterisk/modules/func_env.so
%{_libdir}/asterisk/modules/func_extstate.so
%{_libdir}/asterisk/modules/func_frame_trace.so
%{_libdir}/asterisk/modules/func_global.so
%{_libdir}/asterisk/modules/func_groupcount.so
%{_libdir}/asterisk/modules/func_hangupcause.so
%{_libdir}/asterisk/modules/func_iconv.so
%{_libdir}/asterisk/modules/func_jitterbuffer.so
%{_libdir}/asterisk/modules/func_lock.so
%{_libdir}/asterisk/modules/func_logic.so
%{_libdir}/asterisk/modules/func_math.so
%{_libdir}/asterisk/modules/func_md5.so
%{_libdir}/asterisk/modules/func_module.so
%{_libdir}/asterisk/modules/func_periodic_hook.so
%{_libdir}/asterisk/modules/func_pitchshift.so
%{_libdir}/asterisk/modules/func_pjsip_aor.so
%{_libdir}/asterisk/modules/func_pjsip_contact.so
%{_libdir}/asterisk/modules/func_pjsip_endpoint.so
%{_libdir}/asterisk/modules/func_presencestate.so
%{_libdir}/asterisk/modules/func_rand.so
%{_libdir}/asterisk/modules/func_realtime.so
%{_libdir}/asterisk/modules/func_sha1.so
%{_libdir}/asterisk/modules/func_shell.so
%{_libdir}/asterisk/modules/func_sorcery.so
%{_libdir}/asterisk/modules/func_speex.so
%{_libdir}/asterisk/modules/func_sprintf.so
%{_libdir}/asterisk/modules/func_srv.so
%{_libdir}/asterisk/modules/func_strings.so
%{_libdir}/asterisk/modules/func_sysinfo.so
%{_libdir}/asterisk/modules/func_talkdetect.so
%{_libdir}/asterisk/modules/func_timeout.so
%{_libdir}/asterisk/modules/func_uri.so
%{_libdir}/asterisk/modules/func_version.so
%{_libdir}/asterisk/modules/func_vmcount.so
%{_libdir}/asterisk/modules/func_volume.so
%{_libdir}/asterisk/modules/pbx_ael.so
%{_libdir}/asterisk/modules/pbx_config.so
%{_libdir}/asterisk/modules/pbx_dundi.so
%{_libdir}/asterisk/modules/pbx_loopback.so
%{_libdir}/asterisk/modules/pbx_realtime.so
%{_libdir}/asterisk/modules/pbx_spool.so
%{_libdir}/asterisk/modules/res_adsi.so
%{_libdir}/asterisk/modules/res_ael_share.so
%{_libdir}/asterisk/modules/res_agi.so
%{_libdir}/asterisk/modules/res_ari_applications.so
%{_libdir}/asterisk/modules/res_ari_asterisk.so
%{_libdir}/asterisk/modules/res_ari_bridges.so
%{_libdir}/asterisk/modules/res_ari_channels.so
%{_libdir}/asterisk/modules/res_ari_device_states.so
%{_libdir}/asterisk/modules/res_ari_endpoints.so
%{_libdir}/asterisk/modules/res_ari_events.so
#%{_libdir}/asterisk/modules/res_ari_mailboxes.so
%{_libdir}/asterisk/modules/res_ari_model.so
%{_libdir}/asterisk/modules/res_ari_playbacks.so
%{_libdir}/asterisk/modules/res_ari_recordings.so
%{_libdir}/asterisk/modules/res_ari.so
%{_libdir}/asterisk/modules/res_ari_sounds.so
%{_libdir}/asterisk/modules/res_calendar.so
%{_libdir}/asterisk/modules/res_clialiases.so
%{_libdir}/asterisk/modules/res_clioriginate.so
%{_libdir}/asterisk/modules/res_convert.so
%{_libdir}/asterisk/modules/res_crypto.so
%{_libdir}/asterisk/modules/res_fax.so
%{_libdir}/asterisk/modules/res_format_attr_celt.so
%{_libdir}/asterisk/modules/res_format_attr_h263.so
%{_libdir}/asterisk/modules/res_format_attr_h264.so
%{_libdir}/asterisk/modules/res_format_attr_opus.so
%{_libdir}/asterisk/modules/res_format_attr_silk.so
%{_libdir}/asterisk/modules/res_hep.so
%{_libdir}/asterisk/modules/res_hep_pjsip.so
%{_libdir}/asterisk/modules/res_hep_rtcp.so
%{_libdir}/asterisk/modules/res_http_websocket.so
%{_libdir}/asterisk/modules/res_limit.so
%{_libdir}/asterisk/modules/res_manager_devicestate.so
%{_libdir}/asterisk/modules/res_manager_presencestate.so
%{_libdir}/asterisk/modules/res_monitor.so
%{_libdir}/asterisk/modules/res_musiconhold.so
%{_libdir}/asterisk/modules/res_mutestream.so
%{_libdir}/asterisk/modules/res_parking.so
%{_libdir}/asterisk/modules/res_phoneprov.so
%{_libdir}/asterisk/modules/res_pjsip_acl.so
%{_libdir}/asterisk/modules/res_pjsip_authenticator_digest.so
%{_libdir}/asterisk/modules/res_pjsip_caller_id.so
%{_libdir}/asterisk/modules/res_pjsip_config_wizard.so
%{_libdir}/asterisk/modules/res_pjsip_dialog_info_body_generator.so
%{_libdir}/asterisk/modules/res_pjsip_diversion.so
%{_libdir}/asterisk/modules/res_pjsip_dtmf_info.so
%{_libdir}/asterisk/modules/res_pjsip_endpoint_identifier_anonymous.so
%{_libdir}/asterisk/modules/res_pjsip_endpoint_identifier_ip.so
%{_libdir}/asterisk/modules/res_pjsip_endpoint_identifier_user.so
%{_libdir}/asterisk/modules/res_pjsip_exten_state.so
%{_libdir}/asterisk/modules/res_pjsip_header_funcs.so
%{_libdir}/asterisk/modules/res_pjsip_logger.so
%{_libdir}/asterisk/modules/res_pjsip_messaging.so
%{_libdir}/asterisk/modules/res_pjsip_mwi.so
%{_libdir}/asterisk/modules/res_pjsip_mwi_body_generator.so
%{_libdir}/asterisk/modules/res_pjsip_nat.so
%{_libdir}/asterisk/modules/res_pjsip_notify.so
%{_libdir}/asterisk/modules/res_pjsip_one_touch_record_info.so
%{_libdir}/asterisk/modules/res_pjsip_outbound_authenticator_digest.so
%{_libdir}/asterisk/modules/res_pjsip_outbound_publish.so
%{_libdir}/asterisk/modules/res_pjsip_outbound_registration.so
%{_libdir}/asterisk/modules/res_pjsip_path.so
%{_libdir}/asterisk/modules/res_pjsip_phoneprov_provider.so
%{_libdir}/asterisk/modules/res_pjsip_pidf_body_generator.so
%{_libdir}/asterisk/modules/res_pjsip_pidf_digium_body_supplement.so
%{_libdir}/asterisk/modules/res_pjsip_pidf_eyebeam_body_supplement.so
%{_libdir}/asterisk/modules/res_pjsip_pubsub.so
%{_libdir}/asterisk/modules/res_pjsip_publish_asterisk.so
%{_libdir}/asterisk/modules/res_pjsip_refer.so
%{_libdir}/asterisk/modules/res_pjsip_registrar_expire.so
%{_libdir}/asterisk/modules/res_pjsip_registrar.so
%{_libdir}/asterisk/modules/res_pjsip_rfc3326.so
%{_libdir}/asterisk/modules/res_pjsip_sdp_rtp.so
%{_libdir}/asterisk/modules/res_pjsip_send_to_voicemail.so
%{_libdir}/asterisk/modules/res_pjsip_session.so
%{_libdir}/asterisk/modules/res_pjsip_sips_contact.so
%{_libdir}/asterisk/modules/res_pjsip.so
%{_libdir}/asterisk/modules/res_pjsip_t38.so
%{_libdir}/asterisk/modules/res_pjsip_transport_websocket.so
%{_libdir}/asterisk/modules/res_pjsip_xpidf_body_generator.so
%{_libdir}/asterisk/modules/res_realtime.so
%{_libdir}/asterisk/modules/res_rtp_asterisk.so
%{_libdir}/asterisk/modules/res_rtp_multicast.so
%{_libdir}/asterisk/modules/res_security_log.so
%{_libdir}/asterisk/modules/res_smdi.so
%{_libdir}/asterisk/modules/res_sorcery_astdb.so
%{_libdir}/asterisk/modules/res_sorcery_config.so
%{_libdir}/asterisk/modules/res_sorcery_memory.so
%{_libdir}/asterisk/modules/res_sorcery_realtime.so
%{_libdir}/asterisk/modules/res_speech.so
%{_libdir}/asterisk/modules/res_srtp.so
%{_libdir}/asterisk/modules/res_stasis_answer.so
%{_libdir}/asterisk/modules/res_stasis_device_state.so
%{_libdir}/asterisk/modules/res_stasis_playback.so
%{_libdir}/asterisk/modules/res_stasis_recording.so
%{_libdir}/asterisk/modules/res_stasis_snoop.so
%{_libdir}/asterisk/modules/res_stasis.so
%{_libdir}/asterisk/modules/res_statsd.so
%{_libdir}/asterisk/modules/res_stun_monitor.so
%{_libdir}/asterisk/modules/res_timing_pthread.so
%{_libdir}/asterisk/modules/res_xmpp.so
%{_libdir}/asterisk/modules/app_bridgeaddchan.so
%{_libdir}/asterisk/modules/cdr_radius.so
%{_libdir}/asterisk/modules/cel_radius.so
%{_libdir}/asterisk/modules/chan_console.so
%{_libdir}/asterisk/modules/chan_rtp.so
%{_libdir}/asterisk/modules/codec_resample.so
%{_libdir}/asterisk/modules/format_ogg_speex.so
%{_libdir}/asterisk/modules/func_holdintercept.so
%{_libdir}/asterisk/modules/pbx_lua.so
%{_libdir}/asterisk/modules/res_calendar_caldav.so
%{_libdir}/asterisk/modules/res_calendar_ews.so
%{_libdir}/asterisk/modules/res_calendar_exchange.so
%{_libdir}/asterisk/modules/res_calendar_icalendar.so
%{_libdir}/asterisk/modules/res_config_ldap.so
%{_libdir}/asterisk/modules/res_fax_spandsp.so
%{_libdir}/asterisk/modules/res_format_attr_g729.so
%{_libdir}/asterisk/modules/res_format_attr_ilbc.so
%{_libdir}/asterisk/modules/res_format_attr_siren14.so
%{_libdir}/asterisk/modules/res_format_attr_siren7.so
%{_libdir}/asterisk/modules/res_format_attr_vp8.so
%{_libdir}/asterisk/modules/res_http_media_cache.so
%{_libdir}/asterisk/modules/res_http_post.so
%{_libdir}/asterisk/modules/res_odbc_transaction.so
%{_libdir}/asterisk/modules/res_pjproject.so
%{_libdir}/asterisk/modules/res_pjsip_dlg_options.so
%{_libdir}/asterisk/modules/res_pjsip_empty_info.so
%{_libdir}/asterisk/modules/res_pjsip_history.so
%{_libdir}/asterisk/modules/res_pjsip_transport_management.so
%{_libdir}/asterisk/modules/res_sorcery_memory_cache.so
%{!?el5:%{_libdir}/asterisk/modules/res_timing_timerfd.so}
%{_sbindir}/asterisk
%{_sbindir}/rasterisk
%{_sbindir}/safe_asterisk
#%{_sbindir}/aelparse
%{_sbindir}/astdb2bdb
%{_sbindir}/astdb2sqlite3
%{_sbindir}/astcanary
%{_sbindir}/astgenkey
%{_sbindir}/astversion
%{?_without_newt:%if 0}
%{!?_without_newt:%if 1}
#%{_sbindir}/astman
%endif
%{_sbindir}/autosupport
#%{_sbindir}/check_expr
#%{_sbindir}/conf2ael
#%{_sbindir}/hashtest
#%{_sbindir}/hashtest2
#%{_sbindir}/muted
#%{_sbindir}/smsq
#%{_sbindir}/stereorize
#%{_sbindir}/streamplayer

%{_libdir}/libasteriskssl.so
%{_libdir}/libasteriskssl.so.1
%{_libdir}/libasteriskpj.so
%{_libdir}/libasteriskpj.so.2

#
#  CDR Log Directories
#
%attr(0775,asterisk,asterisk) %dir %{logdir}/asterisk
%attr(0775,asterisk,asterisk) %dir %{logdir}/asterisk/cdr-csv
%attr(0775,asterisk,asterisk) %dir %{logdir}/asterisk/cdr-custom

#
#  logrotate config
#
%config(noreplace) %{_sysconfdir}/logrotate.d/asterisk

#
#  run Directory
#
%attr(0775,asterisk,asterisk) %dir /var/run/asterisk

%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/agi-bin
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/documentation
%attr(0644,asterisk,asterisk)      /var/lib/asterisk/documentation/*
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/images
%attr(0644,asterisk,asterisk)      /var/lib/asterisk/images/*
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/keys
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/licenses
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/phoneprov
%attr(0644,asterisk,asterisk)      /var/lib/asterisk/phoneprov/*
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/rest-api
%attr(0644,asterisk,asterisk)      /var/lib/asterisk/rest-api/*
%attr(0755,asterisk,asterisk) %dir /var/lib/asterisk/static-http
%attr(0644,asterisk,asterisk)      /var/lib/asterisk/static-http/*

%attr(0755,asterisk,asterisk) %dir /var/lib/digium
%attr(0755,asterisk,asterisk) %dir /var/lib/digium/licenses

#
#  Spool Directories
#
%attr(0775,asterisk,asterisk) %dir /var/spool/asterisk
%attr(0775,asterisk,asterisk) %dir /var/spool/asterisk/meetme
%attr(0775,asterisk,asterisk) %dir /var/spool/asterisk/system
%attr(0775,asterisk,asterisk) %dir /var/spool/asterisk/tmp
%attr(0775,asterisk,asterisk) %dir /var/spool/asterisk/voicemail

#
#  Alsa Subpackage
#
%{?_without_alsa:%if 0}
%{!?_without_alsa:%if 1}
%files alsa
%defattr(-, root, root)
%{_libdir}/asterisk/modules/chan_alsa.so
%endif

#
#  snmp Subpackage
#
%{?_without_snmp:%if 0}
%{!?_without_snmp:%if 1}
%files snmp
%defattr(-, root, root)
%{_libdir}/asterisk/modules/res_snmp.so
%endif

#
#  pgsql Subpackage
#
%{?_without_pgsql:%if 0}
%{!?_without_pgsql:%if 1}
%files pgsql
%defattr(-, root, root)
%{_libdir}/asterisk/modules/res_config_pgsql.so
%{_libdir}/asterisk/modules/cdr_pgsql.so
%{_libdir}/asterisk/modules/cel_pgsql.so
%endif

#
#  tds Subpackage
#
%{?_without_tds:%if 0}
%{!?_without_tds:%if 1}
%files tds
%defattr(-, root, root)
%{_libdir}/asterisk/modules/cdr_tds.so
%{_libdir}/asterisk/modules/cel_tds.so
%endif

#
#  mISDN Subpackage
#
%{?_without_misdn:%if 0}
%{!?_without_misdn:%if 1}
%files misdn
%defattr(-, root, root)
%{_libdir}/asterisk/modules/chan_misdn.so
%endif

#
#  dahdi Subpackage
#
%{?_without_dahdi:%if 0}
%{!?_without_dahdi:%if 1}
%files dahdi
%defattr(-, root, root)
/usr/share/dahdi/span_config.d/40-asterisk
%{_libdir}/asterisk/modules/app_dahdiras.so
%{_libdir}/asterisk/modules/app_flash.so
%{_libdir}/asterisk/modules/app_meetme.so
%{_libdir}/asterisk/modules/chan_dahdi.so
%{_libdir}/asterisk/modules/codec_dahdi.so
%{_libdir}/asterisk/modules/res_timing_dahdi.so
%endif

#
#  Configs Subpackage
#
%files configs
%defattr(-, asterisk, asterisk)
%attr(0664,asterisk,asterisk) %config(noreplace) %{_sysconfdir}/asterisk/*

#
#  cURL Subpackage
#
%{?_without_curl:%if 0}
%{!?_without_curl:%if 1}
%files curl
%defattr(-, root, root)
%{_libdir}/asterisk/modules/func_curl.so
%{_libdir}/asterisk/modules/res_config_curl.so
%{_libdir}/asterisk/modules/res_curl.so
%endif

#
#  Development Subpackage
#
%files devel
%defattr(-, root, root)
%{_includedir}/asterisk.h
%{_includedir}/asterisk/_private.h
%{_includedir}/asterisk/abstract_jb.h
%{_includedir}/asterisk/acl.h
%{_includedir}/asterisk/adsi.h
%{_includedir}/asterisk/ael_structs.h
%{_includedir}/asterisk/agi.h
%{_includedir}/asterisk/alaw.h
%{_includedir}/asterisk/aoc.h
%{_includedir}/asterisk/autochan.h
%{_includedir}/asterisk/autoconfig.h
%{_includedir}/asterisk/app.h
%{_includedir}/asterisk/ari.h
%{_includedir}/asterisk/astdb.h
%{_includedir}/asterisk/ast_expr.h
%{_includedir}/asterisk/ast_version.h
%{_includedir}/asterisk/astmm.h
%{_includedir}/asterisk/astobj.h
%{_includedir}/asterisk/astobj2.h
%{_includedir}/asterisk/astosp.h
%{_includedir}/asterisk/audiohook.h
%{_includedir}/asterisk/backtrace.h
%{_includedir}/asterisk/beep.h
%{_includedir}/asterisk/bridge_after.h
%{_includedir}/asterisk/bridge_basic.h
%{_includedir}/asterisk/bridge_channel.h
%{_includedir}/asterisk/bridge_channel_internal.h
%{_includedir}/asterisk/bridge_features.h
%{_includedir}/asterisk/bridge.h
%{_includedir}/asterisk/bridge_internal.h
%{_includedir}/asterisk/bridge_roles.h
%{_includedir}/asterisk/bridge_technology.h
%{_includedir}/asterisk/bucket.h
%{_includedir}/asterisk/build.h
%{_includedir}/asterisk/buildinfo.h
%{_includedir}/asterisk/buildopts.h
%{_includedir}/asterisk/calendar.h
%{_includedir}/asterisk/callerid.h
%{_includedir}/asterisk/causes.h
%{_includedir}/asterisk/ccss.h
%{_includedir}/asterisk/cdr.h
%{_includedir}/asterisk/cel.h
%{_includedir}/asterisk/celt.h
%{_includedir}/asterisk/channel.h
%{_includedir}/asterisk/channel_internal.h
%{_includedir}/asterisk/channelstate.h
%{_includedir}/asterisk/chanvars.h
%{_includedir}/asterisk/cli.h
%{_includedir}/asterisk/codec.h
%{_includedir}/asterisk/compat.h
%{_includedir}/asterisk/compiler.h
%{_includedir}/asterisk/config.h
%{_includedir}/asterisk/config_options.h
%{_includedir}/asterisk/core_local.h
%{_includedir}/asterisk/core_unreal.h
%{_includedir}/asterisk/crypto.h
%{_includedir}/asterisk/data.h
%{_includedir}/asterisk/datastore.h
%{_includedir}/asterisk/devicestate.h
%{_includedir}/asterisk/dial.h
%{_includedir}/asterisk/dlinkedlists.h
%{_includedir}/asterisk/dns.h
%{_includedir}/asterisk/dnsmgr.h
%{_includedir}/asterisk/doxyref.h
%{_includedir}/asterisk/dsp.h
%{_includedir}/asterisk/dundi.h
%{_includedir}/asterisk/endian.h
%{_includedir}/asterisk/endpoints.h
%{_includedir}/asterisk/enum.h
%{_includedir}/asterisk/event.h
%{_includedir}/asterisk/event_defs.h
%{_includedir}/asterisk/extconf.h
%{_includedir}/asterisk/features_config.h
%{_includedir}/asterisk/features.h
%{_includedir}/asterisk/file.h
%{_includedir}/asterisk/format.h
%{_includedir}/asterisk/format_cache.h
%{_includedir}/asterisk/format_compatibility.h
%{_includedir}/asterisk/format_cap.h
%{_includedir}/asterisk/frame.h
%{_includedir}/asterisk/framehook.h
%{_includedir}/asterisk/fskmodem.h
%{_includedir}/asterisk/fskmodem_float.h
%{_includedir}/asterisk/fskmodem_int.h
%{_includedir}/asterisk/global_datastores.h
%{_includedir}/asterisk/hashtab.h
%{_includedir}/asterisk/heap.h
%{_includedir}/asterisk/http.h
%{_includedir}/asterisk/http_websocket.h
%{_includedir}/asterisk/image.h
%{_includedir}/asterisk/indications.h
%{_includedir}/asterisk/inline_api.h
%{_includedir}/asterisk/io.h
%{_includedir}/asterisk/json.h
%{_includedir}/asterisk/linkedlists.h
%{_includedir}/asterisk/localtime.h
%{_includedir}/asterisk/lock.h
%{_includedir}/asterisk/logger.h
%{_includedir}/asterisk/manager.h
%{_includedir}/asterisk/md5.h
%{_includedir}/asterisk/media_index.h
%{_includedir}/asterisk/message.h
%{_includedir}/asterisk/mixmonitor.h
%{_includedir}/asterisk/mod_format.h
%{_includedir}/asterisk/module.h
%{_includedir}/asterisk/monitor.h
%{_includedir}/asterisk/musiconhold.h
%{_includedir}/asterisk/netsock.h
%{_includedir}/asterisk/netsock2.h
%{_includedir}/asterisk/network.h
%{_includedir}/asterisk/optional_api.h
%{_includedir}/asterisk/options.h
%{_includedir}/asterisk/opus.h
%{_includedir}/asterisk/parking.h
%{_includedir}/asterisk/paths.h
%{_includedir}/asterisk/pbx.h
%{_includedir}/asterisk/phoneprov.h
%{_includedir}/asterisk/pickup.h
%{_includedir}/asterisk/pktccops.h
%{_includedir}/asterisk/plc.h
%{_includedir}/asterisk/poll-compat.h
%{_includedir}/asterisk/presencestate.h
%{_includedir}/asterisk/privacy.h
%{_includedir}/asterisk/pval.h
%{_includedir}/asterisk/res_mwi_external.h
%{_includedir}/asterisk/res_odbc.h
%{_includedir}/asterisk/res_fax.h
%{_includedir}/asterisk/res_hep.h
%{_includedir}/asterisk/res_pjsip_cli.h
%{_includedir}/asterisk/res_pjsip.h
%{_includedir}/asterisk/res_pjsip_body_generator_types.h
%{_includedir}/asterisk/res_pjsip_outbound_publish.h
%{_includedir}/asterisk/res_pjsip_presence_xml.h
%{_includedir}/asterisk/res_pjsip_pubsub.h
%{_includedir}/asterisk/res_pjsip_session.h
%{_includedir}/asterisk/res_srtp.h
%{_includedir}/asterisk/rtp_engine.h
%{_includedir}/asterisk/say.h
%{_includedir}/asterisk/sched.h
%{_includedir}/asterisk/sdp_srtp.h
%{_includedir}/asterisk/security_events.h
%{_includedir}/asterisk/security_events_defs.h
%{_includedir}/asterisk/select.h
%{_includedir}/asterisk/sem.h
%{_includedir}/asterisk/sha1.h
%{_includedir}/asterisk/silk.h
%{_includedir}/asterisk/sip_api.h
%{_includedir}/asterisk/slin.h
%{_includedir}/asterisk/slinfactory.h
%{_includedir}/asterisk/smdi.h
%{_includedir}/asterisk/smoother.h
%{_includedir}/asterisk/sorcery.h
%{_includedir}/asterisk/sounds_index.h
%{_includedir}/asterisk/speech.h
%{_includedir}/asterisk/spinlock.h
%{_includedir}/asterisk/srv.h
%{_includedir}/asterisk/stasis_app_device_state.h
%{_includedir}/asterisk/stasis_app.h
%{_includedir}/asterisk/stasis_app_impl.h
%{_includedir}/asterisk/stasis_app_mailbox.h
%{_includedir}/asterisk/stasis_app_playback.h
%{_includedir}/asterisk/stasis_app_recording.h
%{_includedir}/asterisk/stasis_app_snoop.h
%{_includedir}/asterisk/stasis_bridges.h
%{_includedir}/asterisk/stasis_cache_pattern.h
%{_includedir}/asterisk/stasis_channels.h
%{_includedir}/asterisk/stasis_endpoints.h
%{_includedir}/asterisk/stasis.h
%{_includedir}/asterisk/stasis_internal.h
%{_includedir}/asterisk/stasis_message_router.h
%{_includedir}/asterisk/stasis_system.h
%{_includedir}/asterisk/stasis_test.h
%{_includedir}/asterisk/statsd.h
%{_includedir}/asterisk/stringfields.h
%{_includedir}/asterisk/strings.h
%{_includedir}/asterisk/stun.h
%{_includedir}/asterisk/syslog.h
%{_includedir}/asterisk/taskprocessor.h
%{_includedir}/asterisk/tcptls.h
%{_includedir}/asterisk/tdd.h
%{_includedir}/asterisk/term.h
%{_includedir}/asterisk/test.h
%{_includedir}/asterisk/threadpool.h
%{_includedir}/asterisk/threadstorage.h
%{_includedir}/asterisk/time.h
%{_includedir}/asterisk/timing.h
%{_includedir}/asterisk/transcap.h
%{_includedir}/asterisk/translate.h
%{_includedir}/asterisk/udptl.h
%{_includedir}/asterisk/ulaw.h
%{_includedir}/asterisk/unaligned.h
%{_includedir}/asterisk/uri.h
%{_includedir}/asterisk/utils.h
%{_includedir}/asterisk/uuid.h
%{_includedir}/asterisk/vector.h
%{_includedir}/asterisk/version.h
%{_includedir}/asterisk/xml.h
%{_includedir}/asterisk/xmldoc.h
%{_includedir}/asterisk/xmpp.h
%{_includedir}/asterisk/doxygen/architecture.h
%{_includedir}/asterisk/doxygen/asterisk-git-howto.h
%{_includedir}/asterisk/doxygen/commits.h
%{_includedir}/asterisk/doxygen/licensing.h
%{_includedir}/asterisk/doxygen/releases.h
%{_includedir}/asterisk/doxygen/reviewboard.h
%{_includedir}/asterisk/alertpipe.h
%{_includedir}/asterisk/dns_core.h
%{_includedir}/asterisk/dns_internal.h
%{_includedir}/asterisk/dns_naptr.h
%{_includedir}/asterisk/dns_query_set.h
%{_includedir}/asterisk/dns_recurring.h
%{_includedir}/asterisk/dns_resolver.h
%{_includedir}/asterisk/dns_srv.h
%{_includedir}/asterisk/dns_test.h
%{_includedir}/asterisk/dns_tlsa.h
%{_includedir}/asterisk/ilbc.h
%{_includedir}/asterisk/max_forwards.h
%{_includedir}/asterisk/media_cache.h
%{_includedir}/asterisk/multicast_rtp.h
%{_includedir}/asterisk/named_locks.h
%{_includedir}/asterisk/res_odbc_transaction.h
%{_includedir}/asterisk/res_pjproject.h

#
#  Documentation Subpackage
#
%files doc
%defattr(-, root, root)

#
#  Manual Pages
#
%{_mandir}/man8/asterisk.8.gz
%{_mandir}/man8/astgenkey.8.gz
%{_mandir}/man8/autosupport.8.gz
%{_mandir}/man8/safe_asterisk.8.gz
%{_mandir}/man8/astdb2bdb.8.gz
%{_mandir}/man8/astdb2sqlite3.8.gz

#
#  Documents
#
#%{docdir}

#
#  Ogg-Vorbis Subpackage
#
%{?_without_ogg:%if 0}
%{!?_without_ogg:%if 1}
%files ogg
%defattr(-, root, root)
%{_libdir}/asterisk/modules/format_ogg_vorbis.so
%endif

#
#  Speex Subpackage
#
%{?_without_speex:%if 0}
%{!?_without_speex:%if 1}
%files speex
%defattr(-, root, root)
%{_libdir}/asterisk/modules/codec_speex.so
%endif

#
#  Opus Subpackage
#
%if %{with opus}
%files opus
%defattr(-, root, root)
%{_libdir}/asterisk/modules/codec_opus_open_source.so
%{_libdir}/asterisk/modules/format_vp8.so
%endif

#
#  resample Subpackage
#
%{?_without_resample:%if 0}
%{!?_without_resample:%if 1}
%files resample
%defattr(-, root, root)
%{_libdir}/asterisk/modules/codec_resample.so
%endif

#
#  unixODBC Subpackage
#
%{?_without_odbc:%if 0}
%{!?_without_odbc:%if 1}
%files odbc
%defattr(-, root, root)
%{_libdir}/asterisk/modules/cdr_adaptive_odbc.so
%{_libdir}/asterisk/modules/cdr_odbc.so
%{_libdir}/asterisk/modules/cel_odbc.so
%{_libdir}/asterisk/modules/func_odbc.so
%{_libdir}/asterisk/modules/res_config_odbc.so
%{_libdir}/asterisk/modules/res_odbc.so
%endif

#
#  sqlite3 Subpackage
#
%{?_without_sqlite3:%if 0}
%{!?_without_sqlite3:%if 1}
%files sqlite3
%defattr(-, root, root)
%{_libdir}/asterisk/modules/cdr_sqlite3_custom.so
%{_libdir}/asterisk/modules/cel_sqlite3_custom.so
%{_libdir}/asterisk/modules/res_config_sqlite3.so
%endif

#
#  voicemail file storage Subpackage
#
%files voicemail
%defattr(-, root, root)
%{_libdir}/asterisk/modules/app_voicemail.so

#
#  voicemail ODBC storage Subpackage
#
%{?_without_voicemail_odbcstorage:%if 0}
%{!?_without_voicemail_odbcstorage:%if 1}
%files voicemail-odbcstorage
%defattr(-, root, root)
%{_libdir}/asterisk/modules/app_voicemail_odbcstorage.so
%endif

#
#  voicemail IMAP storage Subpackage
#
%{?_without_voicemail_imapstorage:%if 0}
%{!?_without_voicemail_imapstorage:%if 1}
%files voicemail-imapstorage
%defattr(-, root, root)
%{_libdir}/asterisk/modules/app_voicemail_imapstorage.so
%endif

%files addons
%defattr(-, root, root)

%files addons-core
%defattr(-, root, root)
%{_libdir}/asterisk/modules/format_mp3.so

%{?_without_mysql:%if 0}
%{!?_without_mysql:%if 1}
%files addons-mysql
%{_libdir}/asterisk/modules/app_mysql.so
%{_libdir}/asterisk/modules/cdr_mysql.so
%{_libdir}/asterisk/modules/res_config_mysql.so
%endif

%{?_without_bluetooth:%if 0}
%{!?_without_bluetooth:%if 1}
%files addons-bluetooth
%{_libdir}/asterisk/modules/chan_mobile.so
%endif

%{?_without_ooh323:%if 0}
%{!?_without_ooh323:%if 1}
%files addons-ooh323
%{_libdir}/asterisk/modules/chan_ooh323.so
%endif


%changelog
* Sat Nov 18 2017 Corey Farrell <git@cfware.com> - 14.7.2-2
- Remove commented-out files that no longer exist in Asterisk.
- Use %{?dist} instead of _centos7 for release.
- Check for suse_version instead of distname.
- Point "Source:" to asterisk.org download URL and "Source3:" to the
  codec_opus github.com URL so the correct tarballs can be downloaded
  with:
  spectool -g asterisk-open-source-opus.spec

* Fri Nov 17 2017 Nir Simionovich <nir.simionovich+github@gmail.com> - 14.7.2-1
- Update to 14.7.2

* Wed Apr 08 2015 Asterisk Development Team <asteriskteam@digium.com> - 13.3.2-1
- Update to 13.3.2.

* Mon Apr 06 2015 Asterisk Development Team <asteriskteam@digium.com> - 13.3.1-1
- Update to 13.3.1.

* Wed Apr 01 2015 Asterisk Development Team <asteriskteam@digium.com> - 13.3.0-1
- Update to 13.3.0.

* Mon Feb 09 2015 Asterisk Development Team <asteriskteam@digium.com> - 13.2.0-1
- Update to 13.2.0.

* Tue Dec 16 2014 Asterisk Development Team <asteriskteam@digium.com> - 13.1.0-1
- Update to 13.1.0.

* Wed Dec 10 2014 Asterisk Development Team <asteriskteam@digium.com> - 13.0.2-1
- Update to 13.0.2.

* Thu Nov 20 2014 Asterisk Development Team <asteriskteam@digium.com> - 13.0.1-1
- Update to 13.0.1.

* Mon Nov 10 2014 Asterisk Development Team <asteriskteam@digium.com> - 13.0.0-1
- Initial 13 build.
